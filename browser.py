#!/bin/python3


# Load OS functions
import os
from os.path import expanduser

# Load Gtk
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

pwd = expanduser("~")
show_hidden_files = False

win = False


def on_activate(app):
    global win
    win = Gtk.ApplicationWindow(application=app)
    win.set_default_size(width=1920, height=1080)

    win.box = Gtk.Box(spacing=6)
    win.add(win.box)

    win.main_display = Gtk.ScrolledWindow(hadjustment=None, vadjustment=None)
    win.box.pack_start(win.main_display, True, True, 0)

    win.file_container = Gtk.ListBox()
    win.main_display.add(win.file_container)

    mv(pwd)

    win.show_all()


def mv(directory):
    global pwd
    pwd = directory
    win.set_title(directory)
    up_file_row = Gtk.ListBoxRow()
    win.file_container.add(up_file_row)
    up_btn = Gtk.Button()
    up_btn.set_label("..")
    up_file_row.add(up_btn)
    with os.scandir(directory) as files:
        for file in files:
            if (file.name[0]!="." or show_hidden_files==True):
                file_row = Gtk.ListBoxRow()
                win.file_container.add(file_row)
                btn = Gtk.Button()
                btn.set_label(file.name)
                file_row.add(btn)


app = Gtk.Application(application_id='com.lslater.GtkApplication')
app.connect('activate', on_activate)
app.run(None)

